<?php
/**
 * Phoenix wesco theme registration file
 *
 * @category  Phoenix
 * @package   Phoenix/wesco
 * @author    Yuriy Matviyuk <yumat@smile.fr>
 * @copyright 2019 Wesco
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/MyTheme/custom',
    __DIR__
);
