<?php
namespace Test\CustomModule\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;

class Number extends Template implements BlockInterface
{
    protected $_template = "widget/number.phtml";
}