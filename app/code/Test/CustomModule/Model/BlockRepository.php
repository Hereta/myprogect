<?php
namespace Test\CustomModule\Model;

use Magento\Cms\Api\Data\BlockInterface;
use Magento\Cms\Model\BlockRepository as BaseBlockRepository;
use Magento\Cms\Model\ResourceModel\Block\Collection;
use Test\CustomModule\Api\BlockRepositoryInterface;

class BlockRepository extends BaseBlockRepository implements BlockRepositoryInterface
{
    /**
     * @inheritdoc
     */
    public function getByStore($blockIdentifier, $storeId)
    {
        /** @var $collection Collection */
        $collection = $this->blockCollectionFactory->create();
        $collection->addStoreFilter($storeId)
            ->addFilter(BlockInterface::IDENTIFIER, $blockIdentifier)
            ->setPageSize(1)
            ->setCurPage(1);

        return $collection->getSize() ? $collection->getFirstItem() : $this->blockFactory->create();
    }

    /**
     * @inheritdoc
     */
    public function getByIdentifier($blockIdentifier)
    {
        /** @var $collection Collection */
        $collection = $this->blockCollectionFactory->create();
        $collection->addFilter(BlockInterface::IDENTIFIER, $blockIdentifier);

        return $collection;
    }
}
