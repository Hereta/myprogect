<?php
/**
 * Phoenix AddedToCartPopup module.
 * Basic module configuration.
 *
 * @author    Bohdan Harniuk <bohar@smile.fr>
 * @copyright 2019 Wesco
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Test_CustomModule',
    __DIR__
);
