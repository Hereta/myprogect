<?php

namespace Test\CustomModule\Setup\Patch\Schema;

use Magento\Framework\Event\ManagerInterface;
use Magento\Store\Model\GroupFactory;
use Magento\Store\Model\ResourceModel\Group;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Store\Model\ResourceModel\Website;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\WebsiteFactory;
use Magento\Framework\Setup\Patch\DataPatchInterface;


/**
 * Implementation of the notification about MySQL search being deprecated.
 *
 * @see \Magento\ElasticSearch
 */
class StoreInstall implements DataPatchInterface
{

    /**
     * @var WebsiteFactory
     */
    protected $websiteFactory;
    /**
     * @var Website
     */
    protected $websiteResourceModel;
    /**
     * @var StoreFactory
     */
    protected $storeFactory;
    /**
     * @var GroupFactory
     */
    protected $groupFactory;
    /**
     * @var Group
     */
    protected $groupResourceModel;
    /**
     * @var Store
     */
    protected $storeResourceModel;
    /**
     * @var ManagerInterface
     */
    protected $eventManager;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * InstallData constructor.
     * @param WebsiteFactory $websiteFactory
     * @param Website $websiteResourceModel
     * @param Store $storeResourceModel
     * @param Group $groupResourceModel
     * @param StoreFactory $storeFactory
     * @param GroupFactory $groupFactory
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        WebsiteFactory $websiteFactory,
        Website $websiteResourceModel,
        Store $storeResourceModel,
        Group $groupResourceModel,
        StoreFactory $storeFactory,
        GroupFactory $groupFactory,
        StoreManagerInterface $storeManager
    ) {
        $this->websiteFactory = $websiteFactory;
        $this->websiteResourceModel = $websiteResourceModel;
        $this->storeFactory = $storeFactory;
        $this->groupFactory = $groupFactory;
        $this->groupResourceModel = $groupResourceModel;
        $this->storeResourceModel = $storeResourceModel;
        $this->storeManager = $storeManager;
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        /** @var \Magento\Store\Model\Website $website */
        $website = $this->websiteFactory->create();
        $website->load('base');


        /** @var \Magento\Store\Model\Group $group */
        $group = $this->groupFactory->create();
        $group->setWebsiteId($website->getWebsiteId());
        $group->setName('Store Germany');
        $group->setCode('group_de');
        $group->setRootCategoryId(2);
        $group->setDefaultStoreId($this->storeManager->getDefaultStoreView()->getId());
        $this->groupResourceModel->save($group);



        /** @var  \Magento\Store\Model\Store $store */
        $store = $this->storeFactory->create();


        $store->setCode('de_DE');
        $store->setName('Store View Germany');
        $store->setWebsite($website);
        $store->setGroupId($group->getId());
        $store->setData('is_active', 1);
        $this->storeResourceModel->save($store);

        $group->setDefaultStoreId($store->getId());
        $group->save();


    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [];
    }
}
