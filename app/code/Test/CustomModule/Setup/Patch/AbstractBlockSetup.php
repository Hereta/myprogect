<?php

namespace Test\CustomModule\Setup\Patch;

use Exception;
use Magento\Cms\Api\Data\BlockInterface;
use Magento\Cms\Api\Data\BlockInterfaceFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\File\Csv;
use Magento\Framework\Setup\SampleData\FixtureManager;
use Magento\Store\Api\StoreRepositoryInterface;
use Test\CustomModule\Api\BlockRepositoryInterface;

class AbstractBlockSetup
{
    /** @var BlockRepositoryInterface */
    protected $blockRepository;

    /** @var BlockInterfaceFactory */
    protected $blockFactory;

    /** @var FixtureManager */
    protected $fixtureManager;

    /** @var Csv */
    protected $csv;

    /** @var StoreRepositoryInterface */
    protected $storeRepository;

    /**
     * BlockSetup constructor.
     *
     * @param BlockRepositoryInterface $blockRepository
     * @param BlockInterfaceFactory $blockFactory
     * @param FixtureManager $fixtureManager
     * @param Csv $csv
     * @param StoreRepositoryInterface $storeRepository
     */
    public function __construct(
        BlockRepositoryInterface $blockRepository,
        BlockInterfaceFactory $blockFactory,
        FixtureManager $fixtureManager,
        Csv $csv,
        StoreRepositoryInterface $storeRepository
    ) {
        $this->blockRepository = $blockRepository;
        $this->blockFactory = $blockFactory;
        $this->fixtureManager = $fixtureManager;
        $this->csv = $csv;
        $this->storeRepository = $storeRepository;
    }

    /**
     * Get block by identifier and store.
     *
     * @param string $identifier
     * @param int $storeId
     *
     * @return BlockInterface
     */
    protected function getBlockByStore($identifier, $storeId)
    {
        return $this->blockRepository->getByStore($identifier, $storeId);
    }

    /**
     * Install blocks.
     *
     * @param string $fixture
     *
     * @return bool
     * @throws \Exception
     */
    public function installBlocks($fixture)
    {
        $file = $this->fixtureManager->getFixture($fixture);

        if (!file_exists($file)) {
            // @codingStandardsIgnoreStart
            throw new Exception("CSV file '{$fixture}'' not found on the server.");
            // @codingStandardsIgnoreEnd
        }

        $rows = $this->csv->getData($file);
        $headers = array_shift($rows);

        foreach ($rows as $row) {
            $entityData = array_combine($headers, $row);
            $this->installBlock($entityData);
        }

        return true;
    }

    /**
     * Get store Id.
     *
     * @param string $storeCode
     *
     * @return int
     */
    protected function getStoreId($storeCode)
    {
        $storeId = 0;

        if ($storeCode) {
            try {
                $storeId = $this->storeRepository->get($storeCode)->getId();
            } catch (NoSuchEntityException $exception) {
                $storeId = 0;
            }
        }

        return $storeId;
    }

    /**
     * Install block.
     *
     * @param array $data
     *
     * @return bool
     * @SuppressWarnings(PHPMD.EmptyCatchBlock)
     */
    public function installBlock($data)
    {
        $storeId = $this->getStoreId($data['store_code']);
        $block = $this->blockRepository->getByStore($data['identifier'], $storeId);

        unset($data['store_code']);

        foreach ($data as $key => $value) {
            $block->setData($key, $value);
        }

        $block->setData('stores', [$storeId]);

        try {
            $this->blockRepository->save($block);
            // @codingStandardsIgnoreStart
        } catch (LocalizedException $e) {
        }
        // @codingStandardsIgnoreEnd

        return true;
    }
}
