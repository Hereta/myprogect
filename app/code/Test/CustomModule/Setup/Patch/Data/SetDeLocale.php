<?php

namespace Test\CustomModule\Setup\Patch\Data;

use Magento\Framework\Exception\NoSuchEntityException as NoSuchEntityExceptionAlias;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\StoreRepository;
use Magento\Framework\Setup\SampleData\FixtureManager;
use Magento\Framework\File\Csv;
use Test\CustomModule\Setup\Patch\Schema\StoreInstall;
use Magento\Framework\App\Config\Storage\WriterInterface;


class SetDeLocale implements DataPatchInterface
{
    /**
     * @var WriterInterface
     */
    private $writer;

    /**
     * @var FixtureManager
     */
    protected $fixtureManager;

    /**
     * @var Csv
     */
    protected $csv;

    /**
     * @var StoreRepository
     */
    private $storeRepository;

    /**
     * SetDeLocale constructor.
     *
     * @param WriterInterface $writer
     * @param StoreRepository $storeRepository
     * @param FixtureManager $fixtureManager
     * @param Csv $csv
     */
    public function __construct(
        WriterInterface $writer,
        StoreRepository $storeRepository,
        FixtureManager $fixtureManager,
        Csv $csv
    ) {

        $this->writer = $writer;
        $this->fixtureManager = $fixtureManager;
        $this->csv = $csv;
        $this->storeRepository = $storeRepository;
    }


    /**
     * @inheritdoc
     * @throws NoSuchEntityExceptionAlias
     */
    public function apply()
    {
        $this->writer->save(
            'general/locale/code',
            'de_DE',
            'stores',
            $this->storeRepository->get('de_DE')->getId()

        );

        $this->writer->save(
            'design/footer/copyright',
            '©Portofoonweb.nl 2017 All rights reserved.'
        );

        $this->writer->save(
            'web/url/use_store',
            1
        );

    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [StoreInstall::class];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }
}
