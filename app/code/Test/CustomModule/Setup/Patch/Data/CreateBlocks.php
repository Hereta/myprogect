<?php

namespace Test\CustomModule\Setup\Patch\Data;

use Magento\Cms\Api\Data\BlockInterfaceFactory;
use Magento\Framework\File\Csv;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\SampleData\FixtureManager;
use Magento\Store\Api\StoreRepositoryInterface;
use Test\CustomModule\Api\BlockRepositoryInterface;
use Test\CustomModule\Setup\Patch\AbstractBlockSetup;


class CreateBlocks extends AbstractBlockSetup implements DataPatchInterface
{
    /** @var ModuleDataSetupInterface */
    protected $moduleDataSetup;

    /**
     * CreateBlocks constructor.
     *
     * @param BlockRepositoryInterface $blockRepository
     * @param BlockInterfaceFactory $blockFactory
     * @param FixtureManager $fixtureManager
     * @param Csv $csv
     * @param StoreRepositoryInterface $storeRepository
     * @param ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        BlockRepositoryInterface $blockRepository,
        BlockInterfaceFactory $blockFactory,
        FixtureManager $fixtureManager,
        Csv $csv,
        StoreRepositoryInterface $storeRepository,
        ModuleDataSetupInterface $moduleDataSetup
    ) {
        parent::__construct(
            $blockRepository,
            $blockFactory,
            $fixtureManager,
            $csv,
            $storeRepository
        );
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();
        $this->installBlocks('Test_CustomModule::fixtures/blocks/blocks.csv');
        $this->moduleDataSetup->endSetup();
    }

    /**
     * @inheritdoc
     */
    public static function getDependencies()
    {
        return [SetDeLocale::class];
    }

    /**
     * @inheritdoc
     */
    public function getAliases()
    {
        return [];
    }
}
