<?php

namespace Test\CustomModule\Api;

use Magento\Cms\Api\BlockRepositoryInterface as BaseBlockRepositoryInterface;
use Magento\Cms\Api\Data\BlockInterface;
use Magento\Cms\Model\ResourceModel\Block\Collection;

interface BlockRepositoryInterface extends BaseBlockRepositoryInterface
{
    /**
     * Get block by store.
     *
     * @param string $blockIdentifier
     * @param int $storeId
     *
     * @return BlockInterface
     */
    public function getByStore($blockIdentifier, $storeId);

    /**
     * Get block for all stores by identifier
     *
     * @param string $identifier
     *
     * @return Collection
     */
    public function getByIdentifier($identifier);
}
